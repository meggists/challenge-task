# ChallengeTask

I have done a few e2e test to show my expertise. You can add to favourites in detail page. I didn't make error handling because it's small project.

## Run App

Run `npm run install` to install modules for 2 apps, Run `npm run start-angular-app` for a frontend server, then Run `npm run start-server` for a backend server. Navigate to `http://localhost:4200/`.

## Running end-to-end tests

If backend is not running, run `npm run start-server`, then run `npm run e2e` to execute the end-to-end tests via a platform of your choice.

