describe('check Login component', () => {

  it('should route to', () => {
    cy.visit('/');
    cy.get('app-login').should('exist');
  })

  it('should route to registration', () => {
    cy.visit('/');
    cy.get('.registration').click();
    cy.get('app-registration').should('exist');
  });

  it('should register', () => {
    cy.visit('/registration');
    cy.get('.email').type('mail@gmail.com');
    cy.get('.password').type('password');
    cy.get('.email').invoke('val')
      .then(email => {
        cy.get('.password').invoke('val').then(password => {
          cy.get('.submit-button').click();
          cy.get('app-login').should('exist')
        })
      })
  })

  it('should login', () => {
    cy.visit('/registration');
    cy.get('.email').type('mail@gmail.com');
    cy.get('.password').type('password');
    cy.get('.email').invoke('val')
      .then(email => {
        cy.get('.password').invoke('val').then(password => {
          cy.get('.submit-button').click();
          cy.visit('/login');
          cy.get('.email').type('mail@gmail.com');
          cy.get('.password').type('password');
          cy.get('.submit-button').click();
          cy.get('app-wrapper').should('exist');
        })
      })
  })
})
