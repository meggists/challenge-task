import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListComponent} from "./list.component";
import {HouseCardComponent} from "./components/house-card/house-card.component";
import {BookCardComponent} from "./components/book-card/book-card.component";
import {CharacterCardComponent} from "./components/character-card/character-card.component";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterLink} from "@angular/router";
import {StoreModule} from "@ngrx/store";
import {listFeatureKey, listReducer} from "./store/list.reducer";
import {EffectsModule} from "@ngrx/effects";
import {ListEffects} from "./store/list.effects";
import {DetailComponent} from "./components/detail/detail.component";



@NgModule({
  declarations: [ListComponent, HouseCardComponent, BookCardComponent, CharacterCardComponent, DetailComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    StoreModule,
    StoreModule.forFeature(listFeatureKey, listReducer),
    EffectsModule.forFeature([ListEffects]),
    RouterLink
  ]
})
export class ListModule { }
