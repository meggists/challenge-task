import {Component, Input} from '@angular/core';
import {ICharacter} from "../../../../services/app.service";
import {LIST_TYPES, ListItem} from "../../list.model";
import {ListCard} from "../listCard";

@Component({
  selector: 'app-character-card',
  templateUrl: './character-card.component.html',
  styleUrl: './character-card.component.scss'
})
export class CharacterCardComponent extends ListCard {
  @Input() set characterDetail(character: ListItem | null) {
    this.character = (character || {}) as ICharacter
  }

  character!: ICharacter;

  setFavourite() {
    this.setFavouriteChanged.emit({id: this.character.id as number, type: LIST_TYPES.CHARACTERS, name: this.character.name});
  }

  removeFavourite() {
    this.removeFavouriteChanged.emit({id: this.character.id as number, type: LIST_TYPES.CHARACTERS, name: this.character.name})
  }
}
