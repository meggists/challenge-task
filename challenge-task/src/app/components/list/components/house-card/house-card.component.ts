import {Component, Input} from '@angular/core';
import {IHouse} from "../../../../services/app.service";
import {LIST_TYPES, ListItem} from "../../list.model";
import {ListCard} from "../listCard";

@Component({
  selector: 'app-house-card',
  templateUrl: './house-card.component.html',
  styleUrl: './house-card.component.scss'
})
export class HouseCardComponent extends ListCard {
  @Input() set houseDetail(house: ListItem | null) {
    this.house = (house || {}) as IHouse;
  }

  house!: IHouse;

  setFavourite() {
    this.setFavouriteChanged.emit({id: this.house.id as number, type: LIST_TYPES.HOUSES, name: this.house.name});
  }

  removeFavourite() {
    this.removeFavouriteChanged.emit({id: this.house.id as number, type: LIST_TYPES.HOUSES, name: this.house.name})
  }
}
