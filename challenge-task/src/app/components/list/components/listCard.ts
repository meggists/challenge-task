import {Directive, EventEmitter, Input, Output} from "@angular/core";
import {IFavouriteCard} from "../../favourite-pages/favourite-pages.model";

@Directive()

export class ListCard {
  @Input() isOpenedDetail: boolean = false;

  @Output() setFavouriteChanged = new EventEmitter<IFavouriteCard>();
  @Output() removeFavouriteChanged = new EventEmitter<IFavouriteCard>()
}
