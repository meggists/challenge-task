import {Component, OnDestroy, OnInit} from '@angular/core';
import {
  addDetailToFavourite, closeDetail,
  loadBookDetail, loadCharacterDetail,
  loadHouseDetail, removeDetailFromFavourite,
} from "../../store/list.actions";
import {LIST_TYPES, ListItem} from "../../list.model";
import {ActivatedRoute} from "@angular/router";
import {ListState} from "../../store/list.reducer";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {getDetail} from "../../store/list.selects";
import {IFavouriteCard} from "../../../favourite-pages/favourite-pages.model";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrl: './detail.component.scss'
})
export class DetailComponent implements OnInit, OnDestroy {
  item: Observable<ListItem | null> = this.store.select(getDetail)
  listType: LIST_TYPES = LIST_TYPES.HOUSES;

  readonly LIST_TYPES = LIST_TYPES;

  constructor(private activatedRoute: ActivatedRoute,
              private store: Store<ListState>) {
  }

  ngOnInit() {
    this.listType = this.activatedRoute.snapshot.data['type']
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'))
    this.loadData(id, this.listType);
  }

  loadData(id: number, type: LIST_TYPES) {
    switch(type) {
      case LIST_TYPES.BOOKS:
        this.store.dispatch(loadBookDetail({id}));
        break;
      case LIST_TYPES.CHARACTERS:
        this.store.dispatch(loadCharacterDetail({id}));
        break;
      default:
        this.store.dispatch(loadHouseDetail({id}));
    }
  }

  addFavouriteCard(card: IFavouriteCard) {
    this.store.dispatch(addDetailToFavourite({card}));
  }

  removeFavouriteCard(card: IFavouriteCard) {
    this.store.dispatch(removeDetailFromFavourite({card}))
  }

  ngOnDestroy() {
    this.store.dispatch(closeDetail());
  }
}
