import {Component, Input} from '@angular/core';
import {IBook} from "../../../../services/app.service";
import {LIST_TYPES, ListItem} from "../../list.model";
import {ListCard} from "../listCard";

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrl: './book-card.component.scss'
})
export class BookCardComponent extends ListCard {
  @Input() set bookDetail(book: ListItem | null) {
    this.book = (book || {}) as IBook;
  }

  book!: IBook;

  setFavourite() {
    this.setFavouriteChanged.emit({id: this.book.id as number, type: LIST_TYPES.BOOKS, name: this.book.name});
  }

  removeFavourite() {
    this.removeFavouriteChanged.emit({id: this.book.id as number, type: LIST_TYPES.BOOKS, name: this.book.name});
  }
}
