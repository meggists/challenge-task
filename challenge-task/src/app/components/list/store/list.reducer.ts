import {ListData} from "../list.component";
import {Action, createReducer, on} from "@ngrx/store";
import {
  loadList,
  loadFilterItems,
  setSearchQuery,
  loadDetail,
  loadDetailWithFavourite,
  closeDetail
} from "./list.actions";
import {ListItem} from "../list.model";

export const listFeatureKey = 'list';

export interface ListState {
  data: ListData,
  filteredData: ListData;
  searchQuery: string;
  openedDetail: ListItem | null;
}

const initialState: ListState = {
  data: [],
  filteredData: [],
  searchQuery: '',
  openedDetail: null
}

const reducer = createReducer(initialState,
  on(loadList, (state, {data}) => ({...state, data})),
  on(setSearchQuery, (state, {query}) => ({...state, searchQuery: query})),
  on(loadFilterItems, (state) => (
    {...state,
    filteredData: state.data.filter(item => item.name.toLowerCase().includes(state.searchQuery.toLowerCase()))
  })),
  on(loadDetail, (state, {data}) => {
    return ({...state, openedDetail: {...data}})
  }),
  on(loadDetailWithFavourite, (state, {isFavourite}) => (
    {...state, openedDetail: {...state.openedDetail, isFavourite} as ListItem})
  ),
  on(closeDetail, (state) => ({...state, openedDetail: null}))
);

export const listReducer = (state: ListState, action: Action): ListState => reducer(state, action);
