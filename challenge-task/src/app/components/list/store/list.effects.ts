import {Injectable} from "@angular/core";
import {AppService} from "../../../services/app.service";
import {
  addDetailToFavourite,
  getAllBooks,
  getAllCharacters,
  getAllHouses,
  loadBookDetail,
  loadCharacterDetail,
  loadDetail,
  loadDetailWithFavourite,
  loadFilterItems,
  loadHouseDetail,
  loadList,
  removeDetailFromFavourite
} from "./list.actions";
import {exhaustMap, map, of} from "rxjs";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {LIST_TYPES, ListItem} from "../list.model";

@Injectable()
export class ListEffects {

  constructor(private actions$: Actions,
              private appService: AppService) {
  }

  getAllBooks$ = createEffect(() => this.actions$.pipe(
    ofType(getAllBooks),
    exhaustMap(() => this.appService.getAllBooks()
      .pipe(map(books => loadList({data: books}))))
  ))

  getAllCharacters$ = createEffect(() => this.actions$.pipe(
    ofType(getAllCharacters),
    exhaustMap(() => this.appService.getAllCharacters()
      .pipe(map(characters => loadList({data: characters}))))
  ))

  getAllHouses$ = createEffect(() => this.actions$.pipe(
    ofType(getAllHouses),
    exhaustMap(() => this.appService.getAllHouses()
      .pipe(map(houses => loadList({data: houses}))))
  ))

  loadFilteredItems = createEffect(() => this.actions$.pipe(
    ofType(loadList),
    map(() => loadFilterItems())
  ))

  loadBook$ = createEffect(() => this.actions$.pipe(
    ofType(loadBookDetail),
    exhaustMap(({id}) => this.appService.getBook(id)
      .pipe(map((data) => this.mapperLoadDetail(data, LIST_TYPES.BOOKS))))
  ))

  loadCharacter$ = createEffect(() => this.actions$.pipe(
    ofType(loadCharacterDetail),
    exhaustMap(({id}) => this.appService.getCharacter(id)
      .pipe(map((data) => this.mapperLoadDetail(data, LIST_TYPES.CHARACTERS))))
  ))

  loadHouse$ = createEffect(() => this.actions$.pipe(
    ofType(loadHouseDetail),
    exhaustMap(({id}) => this.appService.getHouse(id)
      .pipe(map((data) => this.mapperLoadDetail(data, LIST_TYPES.HOUSES))))

  ))

  addDetailToFavourite$ = createEffect(() => this.actions$.pipe(
    ofType(addDetailToFavourite),
    exhaustMap(({card}) => {
      this.appService.addFavouriteCard(card);
      return of(loadDetailWithFavourite({isFavourite: true}));
    })
  ))

  removeDetailToFavourite$ = createEffect(() => this.actions$.pipe(
    ofType(removeDetailFromFavourite),
    exhaustMap(({card}) => {
      this.appService.removeFavouriteCard(card);
      return of(loadDetailWithFavourite({isFavourite: false}));
    })
  ))

  private mapperLoadDetail = (data: ListItem, type: LIST_TYPES) => {
    const isFavourite: boolean = this.appService.isFavouriteCard(data.id as number, type);
    return loadDetail({data: {...data, isFavourite}})
  }

}
