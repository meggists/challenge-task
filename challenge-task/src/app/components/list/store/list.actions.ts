import {createAction, props} from "@ngrx/store";
import {ListData} from "../list.component";
import {ListItem} from "../list.model";
import {IFavouriteCard} from "../../favourite-pages/favourite-pages.model";

export const getAllBooks = createAction('[List] getAllBooks');
export const getAllCharacters = createAction('[List] getAllCharacters');
export const getAllHouses = createAction('[List] getAllHouses');
export const loadList = createAction('[List] getDataSuccess', props<{data: ListData}>());
export const setSearchQuery = createAction('[List} setSearchQuery', props<{query: string}>());
export const loadFilterItems = createAction('[List] loadFilterItems');
export const loadBookDetail = createAction('[List] loadBookDetail', props<{id: number}>());
export const loadCharacterDetail = createAction('[List] loadCharacterDetail', props<{id: number}>());
export const loadHouseDetail = createAction('[List] loadHouseDetail', props<{id: number}>());
export const loadDetail = createAction('[List] loadDetail', props<{data: ListItem}>());
export const loadDetailWithFavourite = createAction('[List] loadDetailWithFavourite', props<{isFavourite: boolean}>());
export const addDetailToFavourite = createAction('[List] addDetailToFavourite', props<{card: IFavouriteCard}>());
export const removeDetailFromFavourite = createAction('[List] removeDetailFromFavourite', props<{card: IFavouriteCard}>());
export const closeDetail = createAction('[List] closeDetail')
