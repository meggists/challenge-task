import {listFeatureKey, ListState} from "./list.reducer";
import {createFeatureSelector, createSelector} from "@ngrx/store";
import {ListData} from "../list.component";
import {ListItem} from "../list.model";

export const selectFeature = createFeatureSelector<ListState>(listFeatureKey);

export const getListDataSelector = createSelector(
  selectFeature,
  (state: ListState): ListData => state.filteredData
);

export const getDetail = createSelector(
  selectFeature,
  (state: ListState): ListItem | null => state.openedDetail
)
