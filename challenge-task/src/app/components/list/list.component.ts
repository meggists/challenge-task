import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {debounceTime, Observable, switchMap} from "rxjs";
import {LIST_TYPES, ListItem} from "./list.model";
import {FormControl} from "@angular/forms";
import {Store} from "@ngrx/store";
import {getAllBooks, getAllCharacters, getAllHouses, loadFilterItems, setSearchQuery} from "./store/list.actions";
import {getListDataSelector} from "./store/list.selects";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrl: './list.component.scss'
})
export class ListComponent implements OnInit {
  listData$: Observable<ListData> = this.store.select(getListDataSelector)
  listType: LIST_TYPES = LIST_TYPES.HOUSES;
  searchInput: FormControl = new FormControl<string>('');

  readonly LIST_TYPES = LIST_TYPES;

  constructor(private activatedRoute: ActivatedRoute,
              private store: Store){
  }

  ngOnInit() {
    this.listType = this.activatedRoute.snapshot.data['type'];
    this.loadData(this.listType);

    this.searchInput.valueChanges.pipe(debounceTime(300)).subscribe((query: string) => {
      this.store.dispatch(setSearchQuery({ query }));
      this.store.dispatch(loadFilterItems());
    });
   }

  private loadData(listType: LIST_TYPES) {
    switch(listType) {
      case LIST_TYPES.BOOKS:
        this.store.dispatch(getAllBooks());
        break;
      case LIST_TYPES.CHARACTERS:
        this.store.dispatch(getAllCharacters());
        break;
      default:
        this.store.dispatch(getAllHouses());
    }
  }
}

export type ListData = ListItem[];
