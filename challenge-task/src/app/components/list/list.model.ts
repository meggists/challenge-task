import {IBook, ICharacter, IHouse} from "../../services/app.service";

export enum LIST_TYPES {
  BOOKS = 'books',
  CHARACTERS = 'characters',
  HOUSES = 'houses'
}

export type ListItem = IBook | IHouse | ICharacter;
