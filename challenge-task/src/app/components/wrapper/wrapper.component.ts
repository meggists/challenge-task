import { Component } from '@angular/core';
import {isUserAuthorized} from "../auth/store/auth.selects";
import {Store} from "@ngrx/store";
import {AuthState} from "../auth/store/auth.reducers";
import {requestLogout} from "../auth/store/auth.actions";

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrl: './wrapper.component.scss'
})
export class WrapperComponent {
  isLogged$ = this.store.select(isUserAuthorized);

  constructor(private store: Store<AuthState>) {
  }

  logout() {
    this.store.dispatch(requestLogout());
  }
}
