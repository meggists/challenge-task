import { Component } from '@angular/core';
import {AuthState} from "../../store/auth.reducers";
import {Store} from "@ngrx/store";
import {requestLogin} from "../../store/auth.actions";
import {IUserCredentials} from "../../auth.model";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  formGroup: FormGroup = new FormGroup({
    email: new FormControl<string>(''),
    password: new FormControl<string>('')
  });

  login(): void {
    const credentials: IUserCredentials = {...this.formGroup.value};
    this.store.dispatch(requestLogin(credentials));
  }


  constructor(private store: Store<AuthState>) { }
}
