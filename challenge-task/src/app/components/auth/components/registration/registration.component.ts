import { Component } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {IUserCredentials} from "../../auth.model";
import {requestLogin, requestRegister} from "../../store/auth.actions";
import {Store} from "@ngrx/store";
import {AuthState} from "../../store/auth.reducers";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrl: './registration.component.scss'
})
export class RegistrationComponent {
  formGroup: FormGroup = new FormGroup({
    email: new FormControl<string>(''),
    password: new FormControl<string>('')
  });

  signUp(): void {
    const credentials: IUserCredentials = {...this.formGroup.value};
    this.store.dispatch(requestRegister(credentials));
  }


  constructor(private store: Store<AuthState>) { }
}
