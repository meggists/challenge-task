import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {SessionStorageService} from "./session-storage.service";
import {IUserCredentials} from "../auth.model"
import {authApi} from "../../../services/api.service";

@Injectable()

export class AuthService {

  constructor(private httpClient: HttpClient) { }

  login(userInfo: IUserCredentials) {
    return this.httpClient.post(authApi + '/login', userInfo)
  }

  register(userInfo: IUserCredentials): Observable<void> {
    return this.httpClient.post<void>(authApi + '/registration', userInfo)
  }
}
