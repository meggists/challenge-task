import {Injectable} from '@angular/core';

@Injectable()
export class SessionStorageService {

  constructor() { }

  setToken(token: string): void {
    sessionStorage.setItem('token', token);
  }

  getToken(): string {
    return sessionStorage.getItem('token') as string;
  }

  deleteToken(): void {
    sessionStorage.removeItem('token');
  }
}
