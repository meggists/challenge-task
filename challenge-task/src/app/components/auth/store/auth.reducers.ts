import {Action, createReducer, on} from "@ngrx/store";
import {
  requestLogin,
  requestLoginSuccess, requestLogout, requestLogoutSuccess,
  requestRegister,
  requestRegisterSuccess, setToken
} from "./auth.actions";
import {state} from "@angular/animations";

export interface AuthState {
  isAuthorized: boolean,
  token: string,
}

const initialState: AuthState = {
  isAuthorized: false,
  token: '',
}

export const reducer = createReducer(initialState,
  on(requestLoginSuccess, (state, {token}) => ({...state, token, isAuthorized: true})),
  on(requestRegisterSuccess, (state) => ({...state})),
  on(requestLogoutSuccess, (state) => ({...state, isAuthorized: false})),
  on(setToken, (state, {token}) => ({...state, token}))
)

export const authReducer = (state: AuthState, action: Action): AuthState => reducer(state, action);
