import {createFeatureSelector, createSelector} from "@ngrx/store";
import {AuthState} from "./auth.reducers";
import {authFeatureKey} from "../auth.module";

const getAuthState = createFeatureSelector<AuthState>(authFeatureKey);

export const isUserAuthorized = createSelector(
  getAuthState,
  (state: AuthState) => state.isAuthorized
);

export const getToken = createSelector(
  getAuthState,
  (state: AuthState) => state.token
);
