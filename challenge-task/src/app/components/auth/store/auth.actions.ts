import {createAction, props} from "@ngrx/store";
import {IUserCredentials} from "../auth.model";

export const requestLogin = createAction(
  '[Auth] requestLogin',
  props<IUserCredentials>()
);
export const requestLoginSuccess = createAction(
  '[Auth] requestLoginSuccess',
  props<{token: string}>()
);

export const requestRegister = createAction('[Auth] requestRegister', props<IUserCredentials>());
export const requestRegisterSuccess = createAction('[Auth] requestRegisterSuccess');

export const requestLogout = createAction('[Auth] requestLogout');
export const requestLogoutSuccess = createAction('[Auth] requestLogoutSuccess');
export const setToken = createAction('[Auth] setToken', props<{token: string}>());
