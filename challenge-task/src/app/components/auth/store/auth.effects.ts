import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {
  requestLogin,
  requestLoginSuccess, requestLogout, requestLogoutSuccess,
  requestRegister,
  requestRegisterSuccess
} from "./auth.actions";
import {catchError, map, mergeMap, of, tap} from "rxjs";
import {AuthService} from "../services/auth.service";
import {SessionStorageService} from "../services/session-storage.service";
import {Router} from "@angular/router";
import {IUserCredentials} from "../auth.model";

@Injectable()
export class AuthEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(requestLogin),
      mergeMap(({email, password}: IUserCredentials) =>
        this.authService.login({email, password})
          .pipe(map((data: any) => {
              return requestLoginSuccess({token: data.result});
            }),
            catchError(({message}) => {
              alert(message);
              return of(message);
            }))
      )
    )
  )

  loginSuccess$ = createEffect(() =>
      this.actions$.pipe(
        ofType(requestLoginSuccess),
        tap(({token}) => {
          this.sessionStorageService.setToken(token);
          this.router.navigate(['/landing'])
        }),
      ),
    {dispatch: false}
  )

  register$ = createEffect(() =>
    this.actions$.pipe(
      ofType(requestRegister),
      mergeMap(({email, password}: IUserCredentials) =>
        this.authService.register({email, password})
          .pipe(
            map(() => requestRegisterSuccess())
          )
      )
    )
  )

  registerSuccess$ = createEffect(() =>
      this.actions$.pipe(
        ofType(requestRegisterSuccess),
        tap(() => {
          alert('Successfully registered');
          this.router.navigate(['/login'])
        }),
      ),
    {dispatch: false}
  )

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(requestLogout),
      mergeMap(() => of(requestLogoutSuccess()))
    )
  )

  logoutSuccess$ = createEffect(() =>
      this.actions$.pipe(
        ofType(requestLogoutSuccess),
        tap(() => {
          this.sessionStorageService.deleteToken()
          this.router.navigate(['/login'])
        }),
      ),
    {dispatch: false}
  )

  constructor(private actions$: Actions,
              private router: Router,
              private sessionStorageService: SessionStorageService,
              private authService:AuthService) {
  }
}
