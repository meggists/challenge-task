import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {authReducer} from "./store/auth.reducers";
import {LoginComponent} from "./components/login/login.component";
import {AuthEffects} from "./store/auth.effects";
import {RegistrationComponent} from "./components/registration/registration.component";
import {AuthService} from "./services/auth.service";
import {SessionStorageService} from "./services/session-storage.service";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterLink} from "@angular/router";

export const authFeatureKey = 'authFeatureKey';

@NgModule({
  declarations: [LoginComponent, RegistrationComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature(authFeatureKey, authReducer),
    EffectsModule.forFeature([AuthEffects]),
    ReactiveFormsModule,
    RouterLink
  ],
  providers: [AuthService, SessionStorageService]
})
export class AuthModule { }
