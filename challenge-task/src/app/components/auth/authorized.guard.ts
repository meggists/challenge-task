import {map, Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {CanActivate, CanLoad, Route, Router, UrlSegment, UrlTree} from "@angular/router";
import {SessionStorageService} from "./services/session-storage.service";
import {AuthService} from "./services/auth.service";
import {AuthState} from "./store/auth.reducers";
import {Store} from "@ngrx/store";
import {isUserAuthorized} from "./store/auth.selects";
import {requestLoginSuccess, setToken} from "./store/auth.actions";

@Injectable({
  providedIn: 'root'
})
export class AuthorizedGuard implements CanActivate {
  constructor(private authService: AuthService,
              private store: Store<AuthState>,
              private sessionStorageService: SessionStorageService,
              private router: Router) {
  }
  canActivate(): Observable<boolean> | UrlTree {
    const isToken = !!this.sessionStorageService.getToken();
    if (isToken) {
      return this.store.select(isUserAuthorized).pipe(map((isAuthorized: boolean) =>  {
        if (!isAuthorized) {
          this.store.dispatch(setToken({token: this.sessionStorageService.getToken()}));
        }
        return true;
      }))
    }
    return this.router.parseUrl('/login');
  }
}
