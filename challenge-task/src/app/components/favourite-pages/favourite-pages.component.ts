import {Component, OnInit} from '@angular/core';
import {IFavouritePagesState} from "./store/favourite-pages.reducers";
import {Store} from "@ngrx/store";
import {getFavouriteCards} from "./store/favourite-pages.selects";
import {loadFavouriteCards, removeFavouritePage} from "./store/favourite-pages.actions";
import {IFavouriteCard} from "./favourite-pages.model";

@Component({
  selector: 'app-favourite-pages',
  templateUrl: './favourite-pages.component.html',
  styleUrl: './favourite-pages.component.scss'
})
export class FavouritePagesComponent implements OnInit {
  cards$ = this.store.select(getFavouriteCards);

  constructor(private store: Store<IFavouritePagesState>) {
  }

  ngOnInit() {
    this.store.dispatch(loadFavouriteCards());
  }

  removeFavouritePage(card: IFavouriteCard) {
    this.store.dispatch(removeFavouritePage({card}));
  }
}
