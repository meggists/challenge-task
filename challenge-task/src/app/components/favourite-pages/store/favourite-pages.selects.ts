import {createFeatureSelector, createSelector} from "@ngrx/store";
import {IFavouritePagesState} from "./favourite-pages.reducers";
import {favouritePagesKey} from "../favourite-pages.module";
import {IFavouriteCard} from "../favourite-pages.model";

export const selectFeature = createFeatureSelector<IFavouritePagesState>(favouritePagesKey);

export const getFavouriteCards = createSelector(
  selectFeature,
  (state): IFavouriteCard[] => state.favouriteCards
);
