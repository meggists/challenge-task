import {createAction, props} from "@ngrx/store";
import {IFavouriteCard} from "../favourite-pages.model";

export const loadFavouriteCards = createAction('[Favourite Pages] loadCards');
export const loadFavouriteCardsSuccess = createAction('[Favourite Pages] loadCardsSuccess', props<{cards: IFavouriteCard[]}>());
export const addFavouritePage = createAction('[Favourite Pages] addCard', props<{card: IFavouriteCard}>());
export const addFavouritePageSuccess = createAction('[Favourite Pages] addCardSuccess', props<{card: IFavouriteCard}>());
export const removeFavouritePage = createAction('[Favourite Pages] removeFavouritePage', props<{card: IFavouriteCard}>());
export const removeFavouritePageSuccess = createAction('[Favourite Page] removeFavouritePageSuccess', props<{card: IFavouriteCard}>());
