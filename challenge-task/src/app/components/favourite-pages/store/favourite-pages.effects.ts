import {Injectable} from "@angular/core";
import {AppService} from "../../../services/app.service";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {
  addFavouritePage,
  addFavouritePageSuccess,
  loadFavouriteCards,
  loadFavouriteCardsSuccess, removeFavouritePage, removeFavouritePageSuccess
} from "./favourite-pages.actions";
import {exhaustMap, map, mergeMap, of} from "rxjs";

@Injectable()
export class FavouritePagesEffects {

  constructor(private actions$: Actions,
              private appService: AppService) {
  }

  loadCards$ = createEffect(() => this.actions$.pipe(
    ofType(loadFavouriteCards),
    mergeMap(() => {
      const cards = this.appService.getFavouriteCards();
      return of(loadFavouriteCardsSuccess({cards}));
    })
  ))

  addCard$ = createEffect(() => this.actions$.pipe(
    ofType(addFavouritePage),
    mergeMap(({card}) => {
      this.appService.addFavouriteCard(card);
      return of(addFavouritePageSuccess({card}));
    })
  ))

  removeFavouritePage$ = createEffect(() => this.actions$.pipe(
    ofType(removeFavouritePage),
    mergeMap(({card}) => {
      this.appService.removeFavouriteCard(card);
      return of(removeFavouritePageSuccess({card}));
    })
  ))

}
