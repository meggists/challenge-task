import {IFavouriteCard} from "../favourite-pages.model";
import {Action, createReducer, on} from "@ngrx/store";
import {
  addFavouritePageSuccess,
  loadFavouriteCardsSuccess,
  removeFavouritePageSuccess
} from "./favourite-pages.actions";

export interface IFavouritePagesState {
  favouriteCards: IFavouriteCard[];
}

const initialState: IFavouritePagesState = {
  favouriteCards: []
}

const reducer = createReducer(initialState,
  on(loadFavouriteCardsSuccess, (state, {cards}) => ({favouriteCards: [...cards]})),
  on(addFavouritePageSuccess, (state, {card}) => ({favouriteCards: [...state.favouriteCards, card]})),
  on(removeFavouritePageSuccess, (state, {card}) =>
    ({favouriteCards: state.favouriteCards.filter(item => !(item.id === card.id && item.type === card.type))}))
  );

export const favouritePagesReducer = (state: IFavouritePagesState, action: Action): IFavouritePagesState  => reducer(state, action);
