import {LIST_TYPES} from "../list/list.model";

export interface IFavouriteCard {
  type: LIST_TYPES;
  name?: string;
  id: number;
}
