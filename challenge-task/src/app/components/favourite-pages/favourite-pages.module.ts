import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FavouritePagesComponent} from "./favourite-pages.component";
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {FavouritePagesEffects} from "./store/favourite-pages.effects";
import {favouritePagesReducer} from "./store/favourite-pages.reducers";
import {RouterLink} from "@angular/router";

export const favouritePagesKey = 'favourite-pages';

@NgModule({
  declarations: [FavouritePagesComponent],
    imports: [
        CommonModule,
        StoreModule,
        StoreModule.forFeature(favouritePagesKey, favouritePagesReducer),
        EffectsModule.forFeature([FavouritePagesEffects]),
        RouterLink,
    ]
})
export class FavouritePagesModule { }
