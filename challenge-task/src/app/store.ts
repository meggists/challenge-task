import {ListState} from "./components/list/store/list.reducer";

export interface AppState {
  list: ListState;
}

export const reducers: any = [];
