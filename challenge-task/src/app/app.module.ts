import { NgModule, isDevMode } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from "./app.component";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import {routes} from "./app.routes";
import {RouterModule} from "@angular/router";
import {LandingPageComponent} from "./components/landing-page/landing-page.component";
import {ReactiveFormsModule} from "@angular/forms";
import {ListModule} from "./components/list/list.module";
import {reducers} from "./store";
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {FavouritePagesModule} from "./components/favourite-pages/favourite-pages.module";
import {AuthModule} from "./components/auth/auth.module";
import {WrapperComponent} from "./components/wrapper/wrapper.component";


@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    WrapperComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    ListModule,
    FavouritePagesModule,
    AuthModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot({}),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: !isDevMode() })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
