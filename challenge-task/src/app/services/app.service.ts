import {Injectable} from '@angular/core';
import {map, Observable} from "rxjs";
import {ApiService} from "./api.service";
import {LIST_TYPES, ListItem} from "../components/list/list.model";
import {IFavouriteCard} from "../components/favourite-pages/favourite-pages.model";

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private apiService: ApiService) {
  }

  getAllBooks(): Observable<IBook[]> {
    return this.apiService.get<IBook[]>('books')
      .pipe(map(data => data.map(item => this.setId(item) as IBook)));
  }

  getAllCharacters(): Observable<ICharacter[]> {
    return this.apiService.get<ICharacter[]>('characters')
      .pipe(map(data => data.map(item => this.setId(item) as ICharacter)));
  }

  getAllHouses(): Observable<IHouse[]> {
    return this.apiService.get<IHouse[]>('houses')
      .pipe(map(data => data.map(item => this.setId(item) as IHouse)));
  }

  getBook(id: number): Observable<IBook> {
    return this.apiService.get<IBook>(`books/${id}`)
      .pipe(map(data => this.setId(data) as IBook));
  }

  getCharacter(id: number): Observable<ICharacter> {
    return this.apiService.get<ICharacter>(`characters/${id}`)
      .pipe(map(data => this.setId(data) as ICharacter));
  }

  getHouse(id: number): Observable<IHouse> {
    return this.apiService.get<IHouse>(`houses/${id}`)
      .pipe(map(data => this.setId(data) as IHouse));
  }

  getFavouriteCards(): IFavouriteCard[] {
    const jsonData = localStorage.getItem('favourite-cards');
    return jsonData ? JSON.parse(jsonData) : [];
  }

  addFavouriteCard(card: IFavouriteCard) {
    const currentCards = this.getFavouriteCards()

    if (currentCards.some(item => item.id === card.id && item.type === card.type)) {
      return;
    }

    const newCards = [...currentCards, card];
    localStorage.setItem('favourite-cards', JSON.stringify(newCards));
  }

  removeFavouriteCard(card: IFavouriteCard) {
    const currentCards = this.getFavouriteCards()
    const newCards = currentCards
      .filter(item => !(item.id === card.id && item.type === card.type));
    localStorage.setItem('favourite-cards', JSON.stringify(newCards));
  }

  isFavouriteCard(id: number, type: LIST_TYPES): boolean {
    const currentCards = this.getFavouriteCards();
    return currentCards.some(item => item.id === id && item.type === type);
  }

  private setId(item: ListItem): ListItem {
      const splitUrl: string[] = item.url.split('/');
      return {...item, id: Number(splitUrl[splitUrl.length - 1])};
  }
}

export interface IListCard {
  id?: number;
  isFavourite?: boolean;
}

export interface ICharacter extends IListCard {
  aliases: string[];
  allegiances: string[],
  books: string[],
  born: string,
  culture: string,
  died: string,
  father: string,
  mother: string,
  name: string,
  playedBy: string[],
  povBooks: string[],
  spouse: string,
  titles: string[],
  tvSeries: string[],
  url: string;
  gender: string;
}

export interface IBook extends IListCard {
  url: string;
  name: string;
  isbn: string;
  authors: string[];
  numberOfPages: number;
  publisher: string;
  country: string;
  mediaType: string;
  released: string;
  characters: string[];
  povCharacters: string[];
}

export interface IHouse extends IListCard {
  url: string,
  name: string,
  region: string,
  coatOfArms: string,
  words: string,
  titles: string[],
  seats: string[],
  currentLord: string,
  heir: string,
  overlord: string,
  founded: string,
  founder: string,
  diedOut: string,
  ancestralWeapons: string[],
  cadetBranches: string[],
  swornMembers: string[]
}
