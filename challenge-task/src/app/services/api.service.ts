import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  private apiUrl = 'https://anapioficeandfire.com/api/'

  get<T>(url: string): Observable<T> {
    return this.httpClient.get<T>(this.makeFullRequestApi(url));
  }

  private makeFullRequestApi(url: string): string {
    return this.apiUrl + url;
  }
}

export const authApi = 'http://localhost:3000'
