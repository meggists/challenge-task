import { Routes } from '@angular/router';
import {LandingPageComponent} from "./components/landing-page/landing-page.component";
import {ListComponent} from "./components/list/list.component";
import {LIST_TYPES} from "./components/list/list.model";
import {DetailComponent} from "./components/list/components/detail/detail.component";
import {FavouritePagesComponent} from "./components/favourite-pages/favourite-pages.component";
import {LoginComponent} from "./components/auth/components/login/login.component";
import {RegistrationComponent} from "./components/auth/components/registration/registration.component";
import {AuthorizedGuard} from "./components/auth/authorized.guard";
import {AppComponent} from "./app.component";
import {WrapperComponent} from "./components/wrapper/wrapper.component";

export const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: '', component: WrapperComponent, canActivate:[AuthorizedGuard], children: [
      {path: 'landing', component: LandingPageComponent },
      {path: 'favourite', component: FavouritePagesComponent},
      {path: 'list', redirectTo: 'houses'},
      {path: 'books', component: ListComponent, data: {type: LIST_TYPES.BOOKS}},
      {path: 'characters', component: ListComponent, data: {type: LIST_TYPES.CHARACTERS}},
      {path: 'houses', component: ListComponent, data: {type: LIST_TYPES.HOUSES}},
      {path: 'books/detail/:id', component: DetailComponent, data: {type: LIST_TYPES.BOOKS}},
      {path: 'houses/detail/:id', component: DetailComponent, data: {type: LIST_TYPES.HOUSES}},
      {path: 'characters/detail/:id', component: DetailComponent, data: {type: LIST_TYPES.CHARACTERS}}
    ]},
];
