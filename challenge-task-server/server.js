const express = require('express');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const cors = require('cors');
const SECRET_KEY  = 'key';

const app = express();
app.use(bodyParser.json());
const PORT = 3000;

app.use(cors());

const users = [];

app.post('/registration', (req, res) => {
    const { email, password } = req.body;
    if (users.find(user => user.email === email)) {
        return res.status(400).json({ message: 'Username already exists' });
    }

    const newUser = {email, password };
    users.push(newUser);
    res.status(201).json({ message: 'User created successfully', user: newUser });
});

app.post('/login', (req, res) => {
    const { email, password } = req.body;
    const user = users.find(user => user.email === email && user.password === password);
    if (!user) {
        return res.status(401).json({ message: 'Invalid username or password' });
    }
    const token = jwt.sign({email: user.email}, SECRET_KEY);
    res.json({ token });
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
